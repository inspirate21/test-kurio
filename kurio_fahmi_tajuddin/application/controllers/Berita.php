<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Berita_model');

    $this->data['module'] = 'Article';    

  }

  public function index()
  {
    $this->data['title'] = "Data Article";
    
    $this->data['berita_data'] = $this->Berita_model->get_all();
    $this->load->view('back/berita/berita_list', $this->data);
  }

  public function create() 
  {
    $this->data['title']          = 'Add New Article';
    $this->data['action']         = site_url('berita/create_action');
    $this->data['button_submit']  = 'Submit';
    $this->data['button_reset']   = 'Reset';

    $this->data['id_berita'] = array(
      'name'  => 'id_berita',
      'id'    => 'id_berita',
      'type'  => 'hidden',
    );

    $this->data['title_news'] = array(
      'name'  => 'title_news',
      'id'    => 'title_news',
      'type'  => 'text',
      'class' => 'form-control',
      'value' => $this->form_validation->set_value('title_news'),
    );

    $this->data['content'] = array(
      'name'  => 'content',
      'id'    => 'content',      
      'class' => 'form-control',
      'value' => $this->form_validation->set_value('content'),
    );


    $this->data['author'] = array(
      'name'  => 'author',
      'id'    => 'author',
      'type'  => 'text',
      'class' => 'form-control',
      'value' => $this->form_validation->set_value('author'),
    );     
    
    $this->load->view('back/berita/berita_add', $this->data);
  }
  
  public function create_action() 
  {
    $this->load->helper('generate_title_helper');
    $this->_rules();

    if ($this->form_validation->run() == FALSE) 
    {
      $this->create();
    } 
      else 
      {
        /* Jika file upload tidak kosong*/
        /* 4 adalah menyatakan tidak ada file yang diupload*/
        if ($_FILES['thumbnail']['error'] <> 4) 
        {
          $nmfile = generate_title($this->input->post('title_news')); 

          /* memanggil library upload ci */
          $config['upload_path']      = './assets/images/berita/';
          $config['allowed_types']    = 'jpg|jpeg|png|gif';
          // $config['max_size']         = '2048'; // 2 MB
          // $config['max_width']        = '2000'; //pixels
          // $config['max_height']       = '2000'; //pixels
          $config['file_name']        = $nmfile; //nama yang terupload nantinya

          $this->load->library('upload', $config);
          
          if (!$this->upload->do_upload('thumbnail'))
          {   //file gagal diupload -> kembali ke form tambah
              $this->create();
          } 
            //file berhasil diupload -> lanjutkan ke query INSERT
            else 
            { 
              $userfile = $this->upload->data();
              $thumbnail                = $config['file_name']; 
              // library yang disediakan codeigniter
              $config['image_library']  = 'gd2'; 
              // gambar yang akan dibuat thumbnail
              $config['source_image']   = './assets/images/berita/'.$userfile['file_name'].''; 
              // membuat thumbnail
              $config['create_thumb']   = TRUE;               
              // rasio resolusi
              $config['maintain_ratio'] = FALSE; 
              // lebar
              $config['width']          = 400; 
              // tinggi
              $config['height']         = 200; 

              $this->load->library('image_lib', $config);
              $this->image_lib->resize();

              $data = array(
                'title'           => $this->input->post('title_news'),
                'content'         => $this->input->post('content'),
                'author'          => $this->input->post('author'),
                'thumbnail'      => $nmfile,
                'thumbnail_type' => $userfile['file_ext'],
                'thumbnail_size' => $userfile['file_size']
              );

              // eksekusi query INSERT
              $this->Berita_model->insert($data);
              // set pesan data berhasil dibuat
              $this->session->set_flashdata('message', 'Data berhasil dibuat');
              redirect(site_url('berita'));
            }
        }
        else // Jika file upload kosong
        {
          $data = array(
            'title'         => $this->input->post('title_news'),
            'content'       => $this->input->post('content'),
            'author'        => $this->input->post('author')
          );

          // eksekusi query INSERT
          $this->Berita_model->insert($data);
          // set pesan data berhasil dibuat
          $this->session->set_flashdata('message', 'Data berhasil dibuat');
          redirect(site_url('berita'));
        }
      }  
  }
  
  public function update($id) 
  {
    $row = $this->Berita_model->get_by_id($id);
    $this->data['berita'] = $this->Berita_model->get_by_id($id);

    if ($row) 
    {
      $this->data['title']          = 'Update Berita';
      $this->data['action']         = site_url('berita/update_action');
      $this->data['button_submit']  = 'Update';
      $this->data['button_reset']   = 'Reset';

      $this->data['id_berita'] = array(
        'name'  => 'id_berita',
        'id'    => 'id_berita',
        'type'=> 'hidden',
      );

      $this->data['title_news'] = array(
        'name'  => 'title_news',
        'id'    => 'title_news',
        'type'  => 'text',
        'class' => 'form-control',
      );

      $this->data['content'] = array(
        'name'  => 'content',
        'id'    => 'content',      
        'class' => 'form-control',
      );


      $this->data['author'] = array(
        'name'  => 'author',
        'id'    => 'author',
        'type'  => 'text',
        'class' => 'form-control',
      );   

      $this->load->view('back/berita/berita_edit', $this->data);
    } 
      else 
      {
        $this->session->set_flashdata('message', 'Data tidak ditemukan');
        redirect(site_url('berita'));
      }
  }
  
  public function update_action() 
  {
    $this->load->helper('generate_title_helper');
    $this->_rules();

    if ($this->form_validation->run() == FALSE) 
    {
      $this->update($this->input->post('id_berita'));
    } 
      else 
      {
        $nmfile =generate_title($this->input->post('title_news'));
        $id['id_berita'] = $this->input->post('id_berita'); 
        
        /* Jika file upload diisi */
        if ($_FILES['thumbnail']['error'] <> 4) 
        {
          // select column yang akan dihapus (gambar) berdasarkan id
          $this->db->select("thumbnail, thumbnail_type");
          $this->db->where($id);
          $query = $this->db->get('berita');
          $row = $query->row();        

          // menyimpan lokasi gambar dalam variable
          $dir = "assets/images/berita/".$row->thumbnail.$row->thumbnail_type;
          $dir_thumb = "assets/images/berita/".$row->thumbnail.'_thumb'.$row->thumbnail_type;

          // Jika ada foto lama, maka hapus foto kemudian upload yang baru
          if(file_exists($dir) && file_exists($dir_thumb))
          {
            $nmfile =generate_title($this->input->post('title_news'));
            
            // Hapus foto
            unlink($dir);
            unlink($dir_thumb);

            //load uploading file library
            $config['upload_path']      = './assets/images/berita/';
            $config['allowed_types']    = 'jpg|jpeg|png|gif';
            // $config['max_size']         = '2048'; // 2 MB
            // $config['max_width']        = '2000'; //pixels
            // $config['max_height']       = '2000'; //pixels
            $config['file_name']        = $nmfile; //nama yang terupload nantinya

            $this->load->library('upload', $config);
            
            // Jika file gagal diupload -> kembali ke form update
            if (!$this->upload->do_upload('thumbnail'))
            {   
              $this->update();
            } 
              // Jika file berhasil diupload -> lanjutkan ke query INSERT
              else 
              { 
                $userfile = $this->upload->data();
                // library yang disediakan codeigniter
                $thumbnail                = $config['file_name']; 
                //nama yang terupload nantinya
                $config['image_library']  = 'gd2'; 
                // gambar yang akan dibuat thumbnail
                $config['source_image']   = './assets/images/berita/'.$userfile['file_name'].''; 
                // membuat thumbnail
                $config['create_thumb']   = TRUE;               
                // rasio resolusi
                $config['maintain_ratio'] = FALSE; 
                // lebar
                $config['width']          = 400; 
                // tinggi
                $config['height']         = 200; 

                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $data = array(
                  'title'  => $this->input->post('title_news'),
                  'content'    => $this->input->post('content'),
                  'author'        => $this->input->post('author'),
                  'thumbnail'      => $nmfile,
                  'thumbnail_type' => $userfile['file_ext'],
                  'thumbnail_size' => $userfile['file_size'],
                  'time_update'   => date('Y-m-d')
                );

                $this->Berita_model->update($this->input->post('id_berita'), $data);
                $this->session->set_flashdata('message', 'Edit Data Berhasil');
                redirect(site_url('berita'));
              }
          }
            // Jika tidak ada foto pada record, maka upload foto baru
            else
            {
              //load uploading file library
              $config['upload_path']      = './assets/images/berita/';
              $config['allowed_types']    = 'jpg|jpeg|png|gif';
              // $config['max_size']         = '2048'; // 2 MB
              // $config['max_width']        = '2000'; //pixels
              // $config['max_height']       = '2000'; //pixels
              $config['file_name']        = $nmfile; //nama yang terupload nantinya

              $this->load->library('upload', $config);
              
              // Jika file gagal diupload -> kembali ke form update
              if (!$this->upload->do_upload('thumbnail'))
              {   
                $this->update();
              } 
                // Jika file berhasil diupload -> lanjutkan ke query INSERT
                else 
                { 
                  $userfile = $this->upload->data();
                  // library yang disediakan codeigniter
                  $thumbnail                = $config['file_name']; 
                  //nama yang terupload nantinya
                  $config['image_library']  = 'gd2'; 
                  // gambar yang akan dibuat thumbnail
                  $config['source_image']   = './assets/images/berita/'.$userfile['file_name'].''; 
                  // membuat thumbnail
                  $config['create_thumb']   = TRUE;               
                  // rasio resolusi
                  $config['maintain_ratio'] = FALSE; 
                  // lebar
                  $config['width']          = 400; 
                  // tinggi
                  $config['height']         = 200; 

                  $this->load->library('image_lib', $config);
                  $this->image_lib->resize();

                  $data = array(
                    'title'  => $this->input->post('title_news'),
                    'content'    => $this->input->post('content'),
                    'author'        => $this->input->post('author'),
                    'thumbnail'      => $nmfile,
                    'thumbnail_type' => $userfile['file_ext'],
                    'thumbnail_size' => $userfile['file_size'],
                    'time_update'   => date('Y-m-d')
                  );

                  $this->Berita_model->update($this->input->post('id_berita'), $data);
                  $this->session->set_flashdata('message', 'Edit Data Berhasil');
                  redirect(site_url('berita'));
                }
            }
        }
          // Jika file upload kosong
          else 
          {
            $data = array(
              'title'  => $this->input->post('title_news'),
              'content'    => $this->input->post('content'),
              'author'        => $this->input->post('author')
            );

            $this->Berita_model->update($this->input->post('id_berita'), $data);
            $this->session->set_flashdata('message', 'Edit Data Berhasil');
            redirect(site_url('berita'));
          }
      }  
  }
  
  public function delete($id) 
  {
    $row = $this->Berita_model->get_by_id($id);

    $this->db->select("thumbnail, thumbnail_type");
    $this->db->where($row);
    $query = $this->db->get('berita');
    $row2 = $query->row();        

    // menyimpan lokasi gambar dalam variable
    $dir = "assets/images/berita/".$row2->thumbnail.$row2->thumbnail_type;
    $dir_thumb = "assets/images/berita/".$row2->userfile.'_thumb'.$row2->thumbnail_type;

    // Jika data ditemukan, maka hapus foto dan record nya
    if ($row) 
    {
      if(file_exists($dir) && file_exists($dir_thumb)){
        // Hapus foto
        unlink($dir);
        unlink($dir_thumb);
      }
      

      $this->Berita_model->delete($id);
      $this->session->set_flashdata('message', 'Data berhasil dihapus');
      redirect(site_url('berita'));
    } 
      // Jika data tidak ada
      else 
      {
        $this->session->set_flashdata('message', 'Data tidak ditemukan');
        redirect(site_url('berita'));
      }
  }

  public function _rules() 
  {
    $this->form_validation->set_rules('title_news', 'Title', 'trim|required');
    $this->form_validation->set_rules('content', 'Content', 'trim|required');

    // set pesan form validasi error
    $this->form_validation->set_message('required', '{field} wajib diisi');

    $this->form_validation->set_rules('id_berita', 'id_berita', 'trim');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert">', '</div>');
  }

}