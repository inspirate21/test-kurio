<?php $this->load->view('back/head'); ?>
<?php $this->load->view('back/header'); ?>
<?php $this->load->view('back/leftbar'); ?>      

<div class="content-wrapper">
  <section class="content-header">
    <h1><?php echo $title ?></h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url('berita') ?>"><i class="fa fa-newspaper-o"></i> Home</a></li>
      <li><?php echo $module ?></li>
      <li class="active"><a href="<?php echo current_url() ?>"><?php echo $title ?></a></li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-body table-responsive padding">
        <a href="<?php echo base_url('berita/create') ?>">
          <button class="btn btn-success"><i class="fa fa-plus"></i> Add New Article</button>
        </a>
        
        <h4 align="center"><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?></h4>

        <hr/>
        <table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="text-align: center">ID.</th>
              <th style="text-align: center">Title</th>
              <th style="text-align: center">Thumbnail</th>
              <th style="text-align: center">Author</th>
              <!-- <th style="text-align: center">Created Date</th> -->
              <th style="text-align: center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($berita_data as $berita):?>
            <tr>
              <td style="text-align:center"><?php echo $berita->id_berita ?></td>
              <td style="text-align:left"><?php echo $berita->title ?></td>
              
              <td style="text-align:center">
                <?php 
                if(!file_exists("assets/images/berita/".$berita->thumbnail.'_thumb'.$berita->thumbnail_type)) {echo "<img src='".base_url()."assets/images/no_image_thumb.png' width='100'>";}  
                else { echo " <img src='".base_url()."assets/images/berita/".$berita->thumbnail.'_thumb'.$berita->thumbnail_type."' width='100'> ";}
                ?>
              </td>
              <td style="text-align:center"><?php echo $berita->author ?></td> 
              <!-- <td style="text-align:center"><?php echo $berita->time_upload ?></td> -->
              <td style="text-align:center">
              <?php 
              echo anchor(site_url('berita/update/'.$berita->id_berita),'<i class="glyphicon glyphicon-pencil"></i>','title="Edit", class="btn btn-sm btn-warning"'); echo ' ';
              echo anchor(site_url('berita/delete/'.$berita->id_berita),'<i class="glyphicon glyphicon-trash"></i>','title="Hapus", class="btn btn-sm btn-danger", onclick="javasciprt: return confirm(\'Are you sure want to delete this data?\')"');  
              ?>
              </td>
            </tr>
            <?php endforeach;?>
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>

<!-- DATA TABLES SCRIPT -->
<script src="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/datatables/dataTables.bootstrap.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
function confirmDialog() {
  return confirm('Are you sure want to delete this data?');
}
  $('#datatable').dataTable({
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bSort": true,
    "bInfo": true,
    "bAutoWidth": false,
    "aaSorting": [[0,'desc']],
    "lengthMenu": [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "Semua"]],

    "columnDefs": [
        { 
            "targets": [2, -1 ], 
            "orderable": false,
            "searchable": false,
        },
    ],
  });
</script>

<?php $this->load->view('back/footer'); ?>      