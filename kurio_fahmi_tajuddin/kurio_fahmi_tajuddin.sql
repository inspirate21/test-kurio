-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 01 Jan 2018 pada 16.48
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kurio_fahmi_tajuddin`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `author` char(30) NOT NULL,
  `thumbnail` text NOT NULL,
  `thumbnail_type` char(10) NOT NULL,
  `thumbnail_size` int(11) NOT NULL,
  `time_upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `time_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `title`, `content`, `author`, `thumbnail`, `thumbnail_type`, `thumbnail_size`, `time_upload`, `time_update`) VALUES
(21, 'Sir Alex Ferguson: Rekor Arsene Wenger Bakal Abadi', '<p>Eks bos <a href=\"http://www.goal.com/id/tim/manchester-united-fc/6eqit8ye8aomdsrrq0hk3v7gh\">Manchester United</a> Sir Alex Ferguson memprediksi tidak akan pernah ada manajer lain di Liga Primer Inggris yang bisa mematahkan rekor laga milik <a href=\"http://www.goal.com/id/pemain/a-wenger/b6sid2mbbr64omvuf5ui2numt\" target=\"_blank\">Arsene Wenger</a>.</p>\r\n<p>Manajer <a href=\"http://www.goal.com/id/tim/arsenal-fc/4dsgumo7d4zupm2ugsvm4zm4d\" target=\"_blank\">Arsenal</a> itu baru saja mencatatkan rekor baru sebagai manajer dengan jumlah laga Liga Primer terbanyak. Wenger kini sudah memimpin 811 pertandingan Liga Primer sepanjang dua dekade lebih melatih Arsenal.</p>\r\n<p>Pria Prancis berusia 68 tahun itu resmi memecahkan rekor sebelumnya milik Ferguson (810 laga) selepas mendampingi Arsenal dalam laga kontra West Bromwich Albion yang berakhir imbang 1-1, Senin (1/1) dini hari WIB.</p>', 'Goal.com', 'sir-alex-ferguson-rekor-arsene-wenger-bakal-abadi', '.png', 661, '2018-01-01 20:59:23', '2018-01-01 00:00:00'),
(23, 'Sikut Dusan Tadic, Ashley Young Diskors Tiga Laga', '<p>Setelah mendapat rentetan hasil mengecewakan dan badai cedera, <a href=\"http://www.goal.com/id/tim/manchester-united-fc/6eqit8ye8aomdsrrq0hk3v7gh\">Manchester United</a> kembali diterpa masalah baru. Kali ini, United dipastikan tidak bisa diperkuat <a href=\"http://www.goal.com/id/pemain/a-young/7paw998ux29k7hx4t1g0wu85x\">Ashley Young</a> selama tiga laga ke depan akibat sanksi bertanding.</p>\r\n<p>Hukuman ini dijatuhkan Federasi Sepakbola Inggris (FA) kepada Young setelah pemain serbabisa itu kedapatan menyikut perut gelandang <a href=\"http://www.goal.com/id/tim/southampton-fc/d5ydtvt96bv7fq04yqm2w2632\">Southampton</a> <a href=\"http://www.goal.com/id/pemain/d-tadi?/6m2pk6odyb3atayt6u8cifahh\">Dusan Tadic</a>. Kejadian itu terjadi saat keduanya berduel dalam situasi sepak pojok di tengah laga United versus Southampton yang berakhir imbang tanpa gol, Sabtu (30/12).</p>\r\n<p>Young sebetulnya sudah mengajukan permohonan kepada FA agar mereduksi hukuman larangan bermain tiga partai itu. Namun FA menolaknya sehingga Young tetap diskors dalam tiga ke depan melawan Everton (1/1), Derby County di Piala FA (5/1), dan Stoke City (15/1)</p>', 'Sandy Mariatna', 'sikut-dusan-tadic-ashley-young-diskors-tiga-laga', '.png', 380, '2018-01-01 21:25:22', '2018-01-01 00:00:00'),
(29, 'Jose Mourinho: Manchester United Sedang Sial', '<p>Tiga hasil seri beruntun di Liga Primer Inggris tentu saja bukan hal yang menggembirakan bagi <a href=\"http://www.goal.com/id/tim/manchester-united-fc/6eqit8ye8aomdsrrq0hk3v7gh\">Manchester United</a>. Selain kian tertinggal jauh dari pemuncak klasemen Manchester City, rentetan hasil imbang itu juga membuat posisi United melorot ke peringkat ketiga klasemen.</p>\r\n<p>Berturut-turut, United mengalami tren negatif dalam dua pekan terakhir saat melawan Leicester City (2-2), Burnley (2-2), dan Southampton (0-0). Di sela-sela hasil itu, <em>Setan Merah </em>bahkan sempat menelan kekalahan saat menghadapi Bristol City di perempat-final Piala Liga.</p>\r\n<p>Ditanya soal apa penyebab timnya sulit meraih kemenangan, <a href=\"http://www.goal.com/id/pemain/josé-mourinho/dt8gnan8xmg0gh5xv8ezh8amt\">Jose Mourinho</a> memberikan tiga alasan: sedang sial, keputusan penalti yang tidak memihak United, dan banyaknya cedera pemain.</p>', 'Sandy  Goal.com', 'jose-mourinho-manchester-united-sedang-sial-harusnya-dapat-tiga-penalti', '.png', 298, '2018-01-01 22:17:39', '2018-01-01 22:17:39'),
(32, 'test', '<p>nges</p>', 'fahmi', '', '', 0, '2018-01-01 22:45:40', '2018-01-01 22:45:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
